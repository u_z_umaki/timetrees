﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class ShowAllPeople
    {
        static public void RenderingListPeople(List<Person> person)
        {           
            Console.Write("Id");
            RenderingSpase(3);
            Console.Write("Name");
            RenderingSpase(41);
            Console.Write("DataOfBirth\tDataOfDead\t");
            Console.WriteLine("FirstParent   SecondParent");
            for (int i = 0; i < person.Count; i++)
            {
                Console.Write($"{person[i].Id}");
                RenderingSpase(5 - $"{ person[i].Id}".Length);
                Console.Write($"{person[i].Name}");
                RenderingSpase(45 - person[i].Name.Length);
                Console.Write($"{person[i].DateOfBirth.ToString("yyyy.MM.dd")}\t");
                Console.Write($"{person[i].DateOfDead?.ToString("yyyy.MM.dd")}");
                if (person[i].DateOfDead == null)
                {                    
                    RenderingSpase(16);
                }
                else
                {
                    RenderingSpase(6);
                }
                if (person[i].FirstParent == null)
                {
                    RenderingSpase(14);
                }
                else
                {
                    Console.Write($"{person[i].FirstParent.Id}");
                    RenderingSpase(14 - $"{person[i].FirstParent.Id}".Length);
                }
                if (person[i].SecondParent == null)
                {
                    Console.WriteLine("");
                }
                else
                {
                    Console.WriteLine($"{person[i].SecondParent.Id}");
                }
            }
        }
        static private void RenderingSpase(int length)
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write(" ");
            }
        }
    }
}
