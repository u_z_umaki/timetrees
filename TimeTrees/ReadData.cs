﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace TimeTrees
{
    class ReadData
    {
        public void Read(ref List<Person> people, ref List<TimeLineEvent> timeLineEvents)
        {
            IReadFile readFile;
            string[] options = { "csv", "json" };
            IMenu menu = new MenuRendering(options, "Выберите формат файла файла");
            int selectedIndex = menu.Run();
            switch (selectedIndex)
            {
                case 0:
                    readFile = new ReadFileCsv();
                    people = readFile.ReadPeople("..\\..\\..\\..\\people.csv");
                    timeLineEvents = readFile.ReadTimelineEvent("..\\..\\..\\..\\timeline.csv");
                    break;
                case 1:
                    readFile = new ReadFileJson();
                    people = readFile.ReadPeople("..\\..\\..\\..\\people.json");
                    timeLineEvents = readFile.ReadTimelineEvent("..\\..\\..\\..\\timeline.json");
                    break;
            }
        }
    }
    interface IReadFile
    {
        List<Person> ReadPeople(string path);
        List<TimeLineEvent> ReadTimelineEvent(string path);
    }
    class ReadFileCsv : IReadFile
    {
        const byte timelineIndexOfDate = 0;
        const byte timelineIndexOfEvent = 1;
        const byte peopleIndexOfID = 0;
        const byte peopleIndexOfName = 1;
        const byte peopleIndexOfBorn = 2;
        const byte peopleIndexOfDeath = 3;
        const byte peopleIndexFirstParent = 4;
        const byte peopleIndexSecondParent = 5;

        public List<Person> ReadPeople(string path)
        {
            List<Person> people = new List<Person>();

            string[] data = File.ReadAllLines(path);
            for (int i = 0; i < data.Length; i++)
            {
                string[] dataSplitStrind = data[i].Split(';');
                people.Add(new Person()
                {
                    Id = int.Parse(dataSplitStrind[peopleIndexOfID]),
                    Name = dataSplitStrind[peopleIndexOfName],
                    DateOfBirth = DateTime.Parse(dataSplitStrind[peopleIndexOfBorn]),
                    DateOfDead = dataSplitStrind.Length == 3 ? (DateTime?)null : DateHelp.ParseDate(dataSplitStrind[peopleIndexOfDeath]),
                    FirstParent = dataSplitStrind.Length == 5 ? new Person() { Id = int.Parse(dataSplitStrind[peopleIndexFirstParent]) } : null,
                    SecondParent = dataSplitStrind.Length == 6 ? new Person() { Id = int.Parse(dataSplitStrind[peopleIndexSecondParent]) } : null
                });
            }
            return people;
        }
        public List<TimeLineEvent> ReadTimelineEvent(string path)
        {
            List<TimeLineEvent> timeLineEvents = new List<TimeLineEvent>();
            string[] data = File.ReadAllLines(path);
            for (int i = 0; i < data.Length; i++)
            {
                string[] dataSplitStrind = data[i].Split(';');
                timeLineEvents.Add(new TimeLineEvent()
                {
                    events = dataSplitStrind[timelineIndexOfEvent],
                    date = DateHelp.ParseDate(dataSplitStrind[timelineIndexOfDate])
                });

            }
            return timeLineEvents;
        }
    }
    class ReadFileJson : IReadFile
    {
        public List<Person> ReadPeople(string path)
        {
            string read = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<Person>>(read);
        }
        public List<TimeLineEvent> ReadTimelineEvent(string path)
        {
            string read = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<TimeLineEvent>>(read);
        }
    }
}
