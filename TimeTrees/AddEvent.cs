﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TimeTrees
{
    class AddEvent
    {
        public void Add(List<TimeLineEvent> timeLineEvents, List<Person> person)
        {
            ShowAllPeople.RenderingListPeople(person);
            DateTime dateEventJoin;
            do
            {
                try
                {
                    dateEventJoin = DateTime.Parse(DateHelp.AddDate("события"));
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Вы ввели не корректную дату");
                    continue;
                }
            } while (true);
            Console.WriteLine("Введите описание события: ");
            string description = Console.ReadLine();
            AddID(person);
            timeLineEvents.Add(new TimeLineEvent { date = dateEventJoin, events = description });
        }
        private void AddID(List<Person> person)
        {
            Console.WriteLine("Введите ID людей к которым пренадлежит событие.");
            do
            {
                int id = int.Parse(Console.ReadLine());
                if (CheckId(id, person) != true)
                {
                    Console.WriteLine("Человека с таким Id не существует!");
                    //ДОРАБОТКА
                }
                Console.WriteLine("Нажмите Y чтобы ввести ещё. n чтобы закончить ввод.");
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                ConsoleKey keyPressed = keyInfo.Key;
                if (keyPressed == ConsoleKey.N)
                {
                    break;
                }
                if (keyPressed == ConsoleKey.Y)
                {
                    Console.WriteLine();
                    Console.WriteLine("Введите ID");
                    continue;
                }
                else
                {
                    throw new Exception("Такой команды нет!");
                }
            } while (true);
        }
        private bool CheckId(int id, List<Person> person)
        {
            bool checkId = false;
            for (int i = 0; i < person.Count; i++)
            {
                checkId = person[i].Id == id;
                if (checkId == true)
                {
                    break;
                }
            }
            return checkId;
        }
        //private void RenderingListPeople(List<Person> person)
        //{            
        //    Console.WriteLine("Список людей для удобства");
        //    Console.Write($"Id   Name");
        //    //RenderingSpase(41);
        //    Console.WriteLine();
        //    //Console.WriteLine("DataOfBirth\tDataOfDead");
        //    for (int i = 0; i < person.Count; i++)
        //    {
        //        Console.Write($"{person[i].Id}    ");
        //        Console.WriteLine($"{person[i].Name}");
        //        //RenderingSpase(45 - person[i].name.Length);
        //        //Console.WriteLine();
        //        //Console.Write($"{person[i].DataOfBirth.ToString("yyyy.MM.dd")}\t");
        //        //Console.WriteLine($"{person[i].DataOfDead?.ToString("yyyy.MM.dd")}");
        //    }
        //}
        ////private void RenderingSpase(int length)
        ////{
        ////    for (int i = 0; i < length; i++)
        ////    {
        ////        Console.Write(" ");
        ////    }           
        ////}
    }
}
