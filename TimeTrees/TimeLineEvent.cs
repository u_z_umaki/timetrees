﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class TimeLineEvent
    {
        public DateTime date;
        public string events;
        public List<Person> people = new List<Person>();
    }
}
